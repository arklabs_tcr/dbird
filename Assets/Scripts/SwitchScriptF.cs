﻿using UnityEngine;
using System.Collections;

public class SwitchScriptF : MonoBehaviour {

    public SwitchToF s;

	void Start () {

        s = GameObject.Find("Main Camera").GetComponent<SwitchToF>();
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Player")
        { 
            s.switchtf = true;
             Destroy(this.gameObject);
        }

	}
}
