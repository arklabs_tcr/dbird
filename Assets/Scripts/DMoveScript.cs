﻿using UnityEngine;
using System.Collections;
//using UnityStandardAssets.CrossPlatformInput;

public class DMoveScript : MonoBehaviour {
	
	Rigidbody2D r;
	public float speed;
    public float jforce;
	public float radius=.3f;
	private float h;
	private float AbsScaleX;
	private float ColliderSizeX;
	public Transform GroundCheck;
    public Transform GroundCheck2;
    public bool grounded;
	public LayerMask WhatIsGround;
	public float WorldWidth;
	public GameObject CL;
	public GameObject CR;
	public GameObject cam;
	public bool now = false;
	private Animator anim;
	private bool facingright=true;
    public bool jump = false;
    public bool unkillable = false;
    public bool PUActive = false;
    public bool htc = false;
    public float sensitivity = 1f;

    public float velocityy = 0f;

    public GameObject jumpsmokeanim;
   


    public void Start()
	{
		cam = GameObject.Find ("Main Camera");
		AbsScaleX = Mathf.Abs (transform.localScale.x);
		ColliderSizeX = GetComponent<BoxCollider2D> ().size.x;
		WorldWidth = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height)).x;
		anim = gameObject.GetComponent<Animator> ();
		//GroundCheck = transform.Find("GroundCheck");
		r = GetComponent<Rigidbody2D> ();
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
        CamInit ();
	}
	void Update()
	{
        CameraCalc();
        GroundCalc();

        if (Application.isEditor&&htc==false)
            h = Input.GetAxis("Horizontal");
        else
            h = Input.acceleration.x;

        if (grounded&&r.velocity.y<0.01)
        {
            r.velocity = new Vector2(0, jforce);
            Instantiate(jumpsmokeanim, transform.position, Quaternion.identity);
        }
        else
        {
            r.velocity = new Vector2(h * speed, r.velocity.y);
        }
    
        if (h > 0 && !facingright)
            Flip();
		if (h< 0 && facingright)
            Flip ();
    }

    public void GroundCalc()
    {
        grounded = false;
        Collider2D[] colliders = Physics2D.OverlapCircleAll(GroundCheck.position, radius, WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
                grounded = true;
        }
        Collider2D[] colliders2 = Physics2D.OverlapCircleAll(GroundCheck2.position, radius, WhatIsGround);
        for (int i = 0; i < colliders2.Length; i++)
        {
            if (colliders2[i].gameObject != gameObject)
                grounded = true;
        }
    }

    public void CameraCalc()
    {
        if (transform.position.x < (cam.transform.position.x - (WorldWidth - (ColliderSizeX / 2 * AbsScaleX))))
        {
            transform.position = new Vector3(transform.position.x + WorldWidth * 2, transform.position.y);
        }
        if (transform.position.x > (cam.transform.position.x + (WorldWidth - (ColliderSizeX / 2 * AbsScaleX))))
        {
            transform.position = new Vector3(transform.position.x - WorldWidth * 2, transform.position.y);
        }
	}

    public void CamInit()
    {
        CL.transform.position = new Vector3(cam.transform.position.x + (WorldWidth * -2), CL.transform.position.y, -10);
        CR.transform.position = new Vector3(cam.transform.position.x + (WorldWidth * 2), CR.transform.position.y, -10);
    }

	public void Flip()
	{
		facingright = !facingright;
		Vector3 thescale = transform.localScale;
		thescale.x *= -1;
		transform.localScale = thescale;
	}
    
}

