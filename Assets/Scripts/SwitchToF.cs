﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SwitchToF : MonoBehaviour {

	GameObject c;
	CameraScript cs;

	BoxCollider2D dspawncol;

    BoxCollider2D ddestroyercol;
	BoxCollider2D fspawncol;
	BoxCollider2D fdestroyercol;
	BoxCollider2D killbox;

	GameObject Player;
    DMoveScript dscript;
    MoveScript fscript;
	SpawnScript sf;
	Animator anim;
	BoxCollider2D bc;
	PolygonCollider2D pc;
    Rigidbody2D r;
    JetBurnerScript jet;
    public bool switchtf = false;
	void Start()
	{
        jet= GameObject.Find("JetPickup").GetComponent<JetBurnerScript>();
        c = GameObject.Find ("Main Camera");
		cs=c.GetComponent<CameraScript>();
        r= GameObject.Find("Player").GetComponent<Rigidbody2D>();
        dscript =GameObject.Find ("Player").GetComponent<DMoveScript> ();
		fscript=GameObject.Find ("Player").GetComponent<MoveScript> ();
		anim = GameObject.Find ("Player").GetComponent<Animator> ();
		bc=GameObject.Find ("Player").GetComponent<BoxCollider2D> ();
		//pc=GameObject.Find ("Player").GetComponent<PolygonCollider2D> ();
		sf = GameObject.Find ("SpawnF").GetComponent<SpawnScript> ();
		dspawncol=GameObject.Find ("SpawnD").GetComponent<BoxCollider2D>();

		ddestroyercol=GameObject.Find ("DestroyerD").GetComponent<BoxCollider2D>();
		fspawncol=GameObject.Find ("SpawnF").GetComponent<BoxCollider2D>();
		fdestroyercol=GameObject.Find ("DestroyerF").GetComponent<BoxCollider2D>();
		killbox = c.GetComponent<BoxCollider2D> ();

	}
	void Update()
	{
		if (switchtf ==true) {

            jet.flappy=true;
            r.gravityScale = 2;
			cs.assign=true;
			cs.flap = true;
			bc.enabled=false;
			//pc.enabled=true;
			dscript.enabled = false;
			fscript.enabled = true;
			fscript.disabled=false;
			killbox.enabled=false;
			dspawncol.enabled=false;
			
			sf.pos.y=cs.ypos;
			sf.pos.x=cs.xpos+30;
			
			ddestroyercol.enabled=false;
			fspawncol.enabled=true;
			fdestroyercol.enabled=true;
			
            switchtf = false;
	       }
	}
}