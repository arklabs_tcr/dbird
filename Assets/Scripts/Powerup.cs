﻿using UnityEngine;
using System.Collections;

public class Powerup: MonoBehaviour {

    public bool Spring = false;
    public float springforce;
    Rigidbody2D r;
    DMoveScript d;

    public  bool overlap = false;
    float radius = 0.8f;
    public LayerMask WhatIsOverlap;
    public Transform OverlapCheck;
    PolygonCollider2D col;
    BoxCollider2D bcol;

    public bool Ghost;
    public bool unkillable = false;
    void Start()
    {
        d = gameObject.GetComponent<DMoveScript>();
        r = gameObject.GetComponent<Rigidbody2D>();
        col= gameObject.GetComponent<PolygonCollider2D>();
        bcol= gameObject.GetComponent<BoxCollider2D>();
    }
    void Update()
    {

        if (Spring == true)
            SpringMethod();
        overlap = false;
        Collider2D[] colliders2 = Physics2D.OverlapCircleAll(OverlapCheck.position, radius, WhatIsOverlap);
        for (int i = 0; i < colliders2.Length; i++)
        {
            if (colliders2[i].gameObject != gameObject)
                overlap = true;
        }
        if (r.velocity.y < 0 && overlap == false)
        { 
            col.enabled = true;
            bcol.enabled = true;
        }  
    }
    void SpringMethod()
    {
        r.velocity = new Vector2(r.velocity.x, 0);
        r.AddForce(new Vector2(0, springforce));
        col.enabled = false;
        bcol.enabled=false;
        Spring = false;
        d.PUActive = false;
    }
    void GhostMethod()
    {
        Ghost = false;
    }
}
