﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SwitchToD : MonoBehaviour {
	
	GameObject c;
	CameraScript cs;

	BoxCollider2D dspawncol;
	BoxCollider2D ddestroyercol;
	BoxCollider2D fspawncol;
	BoxCollider2D fdestroyercol;
	BoxCollider2D killbox;
	SpawnScriptD sd;

	
	GameObject Player;
	public DMoveScript dscript;
	public MoveScript fscript;
	Animator anim;
	BoxCollider2D bc;
	PolygonCollider2D pc;
    Rigidbody2D r;
	public bool switchtd=false;
    public bool wtf= false;
    void Start()
	{
		c = GameObject.Find ("Main Camera");
		cs=c.GetComponent<CameraScript>();
        r= GameObject.Find("Player").GetComponent<Rigidbody2D>();
        dscript =GameObject.Find ("Player").GetComponent<DMoveScript> ();
		fscript=GameObject.Find ("Player").GetComponent<MoveScript> ();
		bc=GameObject.Find ("Player").GetComponent<BoxCollider2D> ();
		//pc=GameObject.Find ("Player").GetComponent<PolygonCollider2D> ();
	
		dspawncol=GameObject.Find ("SpawnD").GetComponent<BoxCollider2D>();
		sd = GameObject.Find ("SpawnD").GetComponent<SpawnScriptD> ();
		anim = GameObject.Find ("Player").GetComponent<Animator> ();
		ddestroyercol=GameObject.Find ("DestroyerD").GetComponent<BoxCollider2D>();
		fspawncol=GameObject.Find ("SpawnF").GetComponent<BoxCollider2D>();
		fdestroyercol=GameObject.Find ("DestroyerF").GetComponent<BoxCollider2D>();
		killbox = c.GetComponent<BoxCollider2D> ();
		
	}
	void Update()
	{
		if (switchtd== true) {
            wtf = true;

            r.gravityScale = 4;
			cs.flap = false;
			
			dscript.enabled = true;
			dscript.CamInit();
			bc.enabled=true;
			//pc.enabled=false;
			fscript.enabled = false;
			killbox.enabled=true;
			dspawncol.enabled=true;
			
			sd.pos.x=cs.xpos;
			sd.pos.y=cs.ypos+30;
			
			ddestroyercol.enabled=true;
			fspawncol.enabled=false;
			fdestroyercol.enabled=false;
			
            switchtd = false;
            
			
			
		}
	}
}