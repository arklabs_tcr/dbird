﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class KillScriptD : MonoBehaviour {
	
	Powerup p;
	
	void Start()
	{
		p=GameObject.Find ("Player").GetComponent<Powerup> ();
	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player"&&p.Ghost==false) 
		{
			Destroy (other.gameObject);
			SceneManager.LoadScene("Game");
			
		}
	}
}
