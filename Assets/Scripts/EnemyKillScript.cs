﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EnemyKillScript : MonoBehaviour {

	Powerup d;
	void Start()
	{
		d=GameObject.Find ("Player").GetComponent<Powerup> ();
	}
	
	void OnCollisionEnter2D(Collision2D other)
	{

        if (other.collider.gameObject.tag == "Player"&&other.collider.GetType() == typeof(BoxCollider2D))
        {
            Destroy(this.gameObject);
        }
        if (other.collider.gameObject.tag == "Player" && other.collider.GetType() == typeof(PolygonCollider2D)&& d.Ghost == false)
        {
            Destroy(other.gameObject);
            SceneManager.LoadScene("Game1");
        }
		if(other.collider.gameObject.tag == "Player"&&d.Ghost == true) 
		{
			Destroy (this.gameObject);
		}
        
    }
}
