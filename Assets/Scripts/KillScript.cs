﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class KillScript : MonoBehaviour {


	Powerup d;
	
	void Start()
	{
		d=GameObject.Find ("Player").GetComponent<Powerup> ();
	}
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Player"&&d.Ghost==false) 
		{
			Destroy (other.gameObject);
			SceneManager.LoadScene("Game");

		}
	}
}
