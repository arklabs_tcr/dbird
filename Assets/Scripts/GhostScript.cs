﻿using UnityEngine;
using System.Collections;

public class GhostScript : MonoBehaviour {

	public Powerup p;
    public float ghostduration;
	
	void Start()
	{
		p=GameObject.Find ("Player").GetComponent<Powerup> ();
	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") {
            p.Ghost = true;
            p.Invoke("GhostMethod", ghostduration);
			Destroy (this.gameObject);
		}
	}
}
