﻿using UnityEngine;
using System.Collections;

public class ChangeCameraYposScript : MonoBehaviour
{

    CameraScript cs;
    SpawnScriptD sd;
    void Start()
    {
        sd = GameObject.Find("SpawnD").GetComponent<SpawnScriptD>();
        cs = GameObject.Find("Main Camera").GetComponent<CameraScript>();
        cs.ypos = gameObject.transform.position.y;
        sd.pos.y = cs.ypos;
    }
}
