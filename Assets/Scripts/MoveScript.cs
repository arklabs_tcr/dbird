﻿using UnityEngine;
using System.Collections;

public class MoveScript : MonoBehaviour
{

    Rigidbody2D r;
    public float speed;
    public float jspeed;
    public bool jump = false;
    public bool disabled;
    public ParticleSystem Jet;
    public ParticleSystem Jet2;




    void Start()
    {
        r = GetComponent<Rigidbody2D>();

    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            jump = true;
        if (Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonUp(0))
            jump = false;
    }

    void FixedUpdate()
    {
        r.velocity = new Vector2(speed, r.velocity.y);
        if (jump && disabled == false)
        {
            r.AddForce(new Vector2(speed, jspeed));
            Jet.enableEmission=true;
            Jet2.enableEmission = true;
        }
        else
        {
            Jet.enableEmission = false;
            Jet2.enableEmission = false;
        }
       
    }
}