﻿using UnityEngine;
using System.Collections;

public class MovePlatformScript : MonoBehaviour {

	 bool flag=true;
	 float initial=0;
	public float range=0;
	public float MoveSpeed=0.05f;
	 Vector3 thescale;

	void Start () {
	
		initial = transform.position.x;
		thescale = transform.localScale;
	}
	

	void FixedUpdate () {
	
		if (flag == true)
		transform.Translate (MoveSpeed, 0, 0);
		else
		transform.Translate (-MoveSpeed, 0, 0);
		if (transform.position.x > initial + range) {
			thescale.x *= -1;
			transform.localScale = thescale;
			flag = false;
		}
		if (transform.position.x < initial) {
			thescale.x *= -1;
			transform.localScale = thescale;
			flag = true;
		}
	}
}
