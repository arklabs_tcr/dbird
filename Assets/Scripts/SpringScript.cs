﻿using UnityEngine;
using System.Collections;

public class SpringScript : MonoBehaviour {

	public DMoveScript d;
    public Powerup s;
    public Animator anim;
    public bool wtf = false;
	void Start()
	{
		d=GameObject.Find ("Player").GetComponent<DMoveScript> ();
        anim = GameObject.Find("Player").GetComponent<Animator>();
        s = GameObject.Find("Player").GetComponent<Powerup>();
    }
	void OnTriggerEnter2D(Collider2D other)
	{
        wtf = true;
		if (other.gameObject.tag == "Player") {
            d.PUActive = true;
            anim.SetBool("Grounded", false);
            s.Spring = true;
			Destroy(this.gameObject);
		}
	}
}
