﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
	
	
	public GameObject Player;
	public float xaxis;
	public float yaxis;
	public float ypos = 0;
	public float xpos = 0;
	public bool flap=false;
    public float dyaxis;
	public float smooth=5f;
	public bool lerping=false;
	public bool assign=false;
	Vector3 cameraHeight; 
	
	
	void Update () {
		if (flap == true) {
			
			if(this.transform.position.x <(Player.transform.position.x-0.5f))
			{
				lerping=true;
			   transform.position = Vector3.Lerp (transform.position, new Vector3 (Player.transform.position.x + xaxis, ypos, -10), Time.deltaTime * smooth);
			}
			else
			{
				lerping=false;
				transform.position = new Vector3 (Player.transform.position.x+xaxis, ypos, -10);
			}
		}
        else
		 {
			if(this.transform.position.x!=xpos)
			{
				lerping=true;
				transform.position = Vector3.Lerp (transform.position, new Vector3 (xpos, ypos, -10), Time.deltaTime * smooth);
			}
			
			
			   if(Player.transform.position.y-this.transform.position.y>dyaxis)
				{
				    lerping=false;
					this.transform.position =new Vector3 (xpos, Player.transform.position.y - dyaxis, -10); 
				}
	     }
	}

}

