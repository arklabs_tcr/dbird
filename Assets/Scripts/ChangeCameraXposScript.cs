﻿using UnityEngine;
using System.Collections;

public class ChangeCameraXposScript : MonoBehaviour {

    CameraScript cs;
    SpawnScriptD sd;
	void Start () {
        sd = GameObject.Find("SpawnD").GetComponent<SpawnScriptD>();
        cs = GameObject.Find("Main Camera").GetComponent<CameraScript>();
        cs.xpos = gameObject.transform.position.x + 5;
        sd.pos.x = cs.xpos;
    }
}
