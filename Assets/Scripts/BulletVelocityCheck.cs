﻿using UnityEngine;
using System.Collections;

public class BulletVelocityCheck : MonoBehaviour {

    Rigidbody2D r;
    public GameObject smokepuff;
	void Start () {
        r = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {

        if (r.velocity.y < 0 )
        {
            Destroy(gameObject);
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Instantiate(smokepuff, other.gameObject.transform.position, Quaternion.identity);
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
          
    }
}
