﻿using UnityEngine;
using System.Collections;

public class SpawnScriptD : MonoBehaviour {
	
	public GameObject[] obj;
	public GameObject Spike;
	public float spacing=10f;
	public Vector3 pos=new Vector3();
	Vector3 posp=new Vector3();
	public int begin=0;
	public int end=20;
	public int difficulty=20;
    public int sdist=150;
	public float ypos;
	public int n=0;
    public int m = 0;
    public bool change;
    BoxCollider2D col;

	void Start()
	{
		pos.x = 0;
		pos.y = 30;
		pos.z = 0.5f;
        col = gameObject.GetComponent<BoxCollider2D>();
	}
	public void Const()
	{
	}
	void Update()
	{
		ypos = transform.position.y-(difficulty*n);
		if (ypos>difficulty) 
		{
			if(begin<10)
				begin+=1;
			if(end<15)
			end+=1;
			n+=1;
		}
        ypos = transform.position.y - (sdist * m);
        if (ypos > sdist)
        {
            change= true;
            m += 1;
        }

    }
	
	void OnTriggerExit2D(Collider2D other)
	{
		
		if (change==true)
        {

            Instantiate (Spike, pos, Spike.transform.rotation);
			pos.y+=spacing;
            change = false;
            col.enabled = false;
		}
		else if(other.gameObject.tag == "Background")//&&transitioning==false)
        {
			Instantiate (obj [Random.Range (begin, end)], pos,obj [Random.Range (begin, end)].transform.rotation);
			pos.y += spacing;
		}
	}
}