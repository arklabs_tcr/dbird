﻿using UnityEngine;
using System.Collections;

public class JetBurnerScript : MonoBehaviour {

    public bool flappy = false;
    public SpriteRenderer a;
    public SpriteRenderer b;
    public ParticleSystem aa;
    public ParticleSystem bb;
	void Start()
    {
        ParticleSystem.EmissionModule em = aa.emission;
        em.enabled = false;
        ParticleSystem.EmissionModule em2 = bb.emission;
        em2.enabled = false;
    }
	void Update () {
	
        if(flappy==true)
        {
            a.enabled = true;
            b.enabled = true;
            ParticleSystem.EmissionModule em = aa.emission;
            em.enabled = true;
            ParticleSystem.EmissionModule em2 = bb.emission;
            em2.enabled = true;
            flappy = false;
        }
	}
}
