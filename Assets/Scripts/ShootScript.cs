﻿using UnityEngine;
using System.Collections;

public class ShootScript : MonoBehaviour
{
    public GameObject projectile;
    public GameObject clone;
    public Transform BulletPosition;
    public Ray r;
    public Vector3 pos;
    Rigidbody2D rig;
    public float force = 0f;
    public Vector3 dir;
    void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            r = Camera.main.ScreenPointToRay(Input.mousePosition);
            pos = r.origin + (r.direction * 10);
            pos.z = 0;
            
            dir=(pos - BulletPosition.transform.position).normalized;
            if((dir.x/dir.y)<1 && (dir.x / dir.y)>-1)
            {
                clone = Instantiate(projectile, BulletPosition.position, transform.rotation) as GameObject;
                clone.transform.rotation = Quaternion.LookRotation(Vector3.forward, pos - clone.transform.position);
                clone.GetComponent<Rigidbody2D>().AddForce(dir * force);

            }
            

        }
                
        
    }
}