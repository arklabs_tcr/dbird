﻿using UnityEngine;
using System.Collections;

public class ShurikenRotateScript : MonoBehaviour {


    public float speed;
	void Update () {

        transform.Rotate(transform.forward * speed*Time.deltaTime);
	}
    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
