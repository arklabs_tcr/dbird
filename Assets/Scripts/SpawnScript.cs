﻿using UnityEngine;
using System.Collections;

public class SpawnScript : MonoBehaviour {

	public GameObject[] obj;
	public GameObject SpikeF;
	public float spacing=25f;
	public Vector3 pos=new Vector3();
	public CameraScript c;

	public int begin=0;
	public int end=20;
	public int difficulty=20;
	public float xpos;
	public int n=0;
    public int m = 0;
	public float sdist=100;
	public float originalswitchd;
	public bool transitioning=false;
    public bool wtf;
    public bool change = false;
    BoxCollider2D col;

	void Start()
	{
		pos.x = 25;
		c = GameObject.Find ("Main Camera").GetComponent<CameraScript> ();
		pos.z = 0.5f;
        col = gameObject.GetComponent<BoxCollider2D>();
	}
	void Update()
	{
		xpos = transform.position.x-(difficulty*n);
		if (xpos>difficulty) 
		{
			if(begin<10)
				begin+=1;
			if(end<15)
				end+=1;
			n+=1;
		}
        xpos = transform.position.x - (sdist * m);
        if (xpos > sdist)
        {
            change = true;
            m += 1;
        }

    }
	void OnTriggerExit2D(Collider2D other)
	{
        wtf = !wtf;
		if (change==true)
        {
            pos.y = c.ypos ;
            Instantiate (SpikeF, pos, SpikeF.transform.rotation);
			pos.x+=spacing;
            change = false;
            col.enabled = false;
			
		
		}
		else if(other.gameObject.tag == "Background")
        {
			pos.y = c.ypos; 
			Instantiate (obj [Random.Range (begin, end)], pos,obj [Random.Range (begin, end)].transform.rotation);
			pos.x += spacing;
        }
	}
}