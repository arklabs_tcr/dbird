
spineboy.png
format: RGBA8888
filter: Linear,Linear
repeat: none
arm
  rotate: true
  xy: 264, 2
  size: 55, 33
  orig: 57, 35
  offset: 1, 1
  index: -1
arm copy
  rotate: false
  xy: 323, 81
  size: 48, 45
  orig: 50, 47
  offset: 1, 1
  index: -1
body
  rotate: false
  xy: 264, 59
  size: 57, 67
  orig: 59, 69
  offset: 1, 1
  index: -1
fist
  rotate: true
  xy: 453, 92
  size: 34, 40
  orig: 36, 42
  offset: 1, 1
  index: -1
fist copy
  rotate: false
  xy: 328, 2
  size: 31, 39
  orig: 33, 41
  offset: 1, 1
  index: -1
head
  rotate: false
  xy: 154, 27
  size: 108, 99
  orig: 110, 101
  offset: 1, 1
  index: -1
jet
  rotate: true
  xy: 2, 30
  size: 96, 150
  orig: 108, 163
  offset: 6, 6
  index: -1
leg
  rotate: false
  xy: 413, 89
  size: 38, 37
  orig: 40, 39
  offset: 1, 1
  index: -1
leg copy
  rotate: false
  xy: 373, 81
  size: 38, 45
  orig: 40, 47
  offset: 1, 1
  index: -1
lrg
  rotate: false
  xy: 328, 43
  size: 37, 36
  orig: 39, 38
  offset: 1, 1
  index: -1
lrg copy
  rotate: true
  xy: 367, 50
  size: 29, 38
  orig: 31, 40
  offset: 1, 1
  index: -1
pelvis
  rotate: false
  xy: 2, 2
  size: 54, 26
  orig: 56, 28
  offset: 1, 1
  index: -1
shoe
  rotate: false
  xy: 58, 9
  size: 46, 19
  orig: 48, 21
  offset: 1, 1
  index: -1
shoe copy
  rotate: true
  xy: 299, 10
  size: 47, 27
  orig: 49, 29
  offset: 1, 1
  index: -1
